$(window).ready(function(){

  // Scroll position. Change to control when to change background to blue
  var target = 900;

  $(function() {
     $(window).scroll(function () {
        if ($(this).scrollTop() > target) {
           $('.hundred:nth-child(3)')
           .removeClass('removeColor')
           .addClass('changeColor')
        }
        if ($(this).scrollTop() < target) {
           $('.hundred:nth-child(3)')
           .removeClass('changeColor')
           .addClass('removeColor')
        }
     });
  });

  $(function () {
      $('.textcarousel').marquee({
           duration: 9000,
           duplicated: true,
           direction: 'left',
           pauseOnHover: true,
           loop: 'infinite'
      });
  });
});
